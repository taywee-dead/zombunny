/*
 *  Lead Coder:     Taylor C. Richberger
 */

#ifndef MAINGAME_STATE_CXX
#define MAINGAME_STATE_CXX
#include <cstdlib>
#include <iostream>

#include "maingamestate.hxx"
#include "pausemenustate.hxx"

MainGameState::MainGameState(Core* game)
{
    this->game = game;
}

void MainGameState::Initiate()
{
    showVision = false;
    velocityIterations = 6;
    positionIterations = 2;
    gravity = b2Vec2(0.0f, 0.0f);
    doSleep = true;
    world = new b2World(gravity);
    world->SetAllowSleeping(doSleep);
    contactListener = new ContactListener();
    world->SetContactListener(contactListener);

    scaleFactor = 5.0f;
    speedFactor = 1.0f;

    shapeList = new std::vector<Shape*>();
    bunnyList = new std::vector<Bunny*>();

    shapeList->push_back(Shape::CreateRectangle(1.0f, 60.0f, 1.0f, 60.0f, false, world));
    shapeList->push_back(Shape::CreateRectangle(150.0f, 60.0f, 1.0f, 60.0f, false, world));
    shapeList->push_back(Shape::CreateRectangle(75.0f, 1.0f, 75.0f, 1.0f, false, world));
    shapeList->push_back(Shape::CreateRectangle(75.0f, 120.0f, 75.0f, 1.0f, false, world));


    for (unsigned short int i = 0; i < 200; i++)
    {
        bunnyList->push_back(new Bunny());
        bunnyList->back()->Create(rand() % 100 + 2, rand() % 100 + 2, (bool)(rand() % 2), !(bool)(rand() % 10), world);
    }
}

void MainGameState::Cleanup()
{
    while (!bunnyList->empty())
    {
        bunnyList->back()->Cleanup();
        delete bunnyList->back();
        bunnyList->pop_back();
    }
    while (!shapeList->empty())
    {
        shapeList->back()->Cleanup();
        delete shapeList->back();
        shapeList->pop_back();
    }

    delete shapeList;
    delete bunnyList;
    delete world;
    delete contactListener;
}

void MainGameState::Pause()
{
    for (unsigned short int i = 0; i < bunnyList->size(); i++)
    {
        bunnyList->at(i)->Pause();
    }
    for (unsigned short int i = 0; i < shapeList->size(); i++)
    {
        shapeList->at(i)->Pause();
    }
}

void MainGameState::Resume()
{
    for (unsigned short int i = 0; i < bunnyList->size(); i++)
    {
        bunnyList->at(i)->Resume();
    }
    for (unsigned short int i = 0; i < shapeList->size(); i++)
    {
        shapeList->at(i)->Resume();
    }
}

void MainGameState::HandleEvents(sf::Event* event)
{
    switch (event->Type)
    {
        case sf::Event::KeyPressed:
            switch (event->Key.Code)
            {
                case sf::Keyboard::Up:
                    speedFactor *= (5.0f / 4.0f);
                    break;
                case sf::Keyboard::Down:
                    speedFactor *= (4.0f / 5.0f);
                    break;
                case sf::Keyboard::Left:
                    break;
                case sf::Keyboard::Right:
                    break;
                case sf::Keyboard::Return:
                    speedFactor = 1.0f;
                    break;
                case sf::Keyboard::Space:
                    showVision = !showVision;
                    break;
                case sf::Keyboard::Escape:
                    game->PushState(new PauseMenuState(game));
                default:
                    break;
            }
            break;
        case sf::Event::KeyReleased:
            switch (event->Key.Code)
            {
                case sf::Keyboard::Up:
                    break;
                case sf::Keyboard::Down:
                    break;
                case sf::Keyboard::Left:
                    break;
                case sf::Keyboard::Right:
                    break;
                case sf::Keyboard::Space:
                    break;
                default:
                    break;
            }
            break;
        case sf::Event::MouseButtonPressed:
            switch (event->MouseButton.Button)
            {
                case sf::Mouse::Left:
                    bunnyList->push_back(new Bunny());
                    bunnyList->back()->Create(event->MouseButton.X / scaleFactor, event->MouseButton.Y / scaleFactor, (bool)(rand() % 2), false, world);
                    break;
                case sf::Mouse::Right:
                    bunnyList->push_back(new Bunny());
                    bunnyList->back()->Create(event->MouseButton.X / scaleFactor, event->MouseButton.Y / scaleFactor, (bool)(rand() % 2), true, world);
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }
}

void MainGameState::Process(float frameTime)
{
    world->Step(frameTime * speedFactor, velocityIterations, positionIterations);
    for (unsigned short int i = 0; i < bunnyList->size(); i++)
    {
        bunnyList->at(i)->Process();
    }
    for (unsigned short int i = 0; i < shapeList->size(); i++)
    {
        shapeList->at(i)->Process();
    }
}

void MainGameState::Render(sf::RenderWindow* window)
{
    for (unsigned short int i = 0; i < shapeList->size(); i++)
    {
        shapeList->at(i)->Render(scaleFactor, window);
    }
    if (showVision)
    {
        for (unsigned short int i = 0; i < bunnyList->size(); i++)
        {
            bunnyList->at(i)->RenderVision(scaleFactor, window);
        }
    }

    for (unsigned short int i = 0; i < bunnyList->size(); i++)
    {
        bunnyList->at(i)->Render(scaleFactor, window);
    }
}

#endif

