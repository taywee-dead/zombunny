/*
 *  Lead Coder:     Taylor C. Richberger
 */

#ifndef BUNNY_HXX
#define BUNNY_HXX

#include <cstdlib>
#include <cmath>
#include <vector>
#include <Box2D/Box2D.h>
#include <SFML/Graphics.hpp>

class Bunny
{
    private:
        const static float PI = 3.1415926535;

        b2Body* body;
        b2Fixture* bunnyFixture;
        b2Fixture* visionFixture;

        sf::Shape renderBunny;
        sf::Shape renderVision;

        bool female;
        bool zombie;
        bool seesZombie;
        bool seesBunny;
        std::vector<Bunny*>* visibleBunnies;
        std::vector<Bunny*>* contactBunnies;


        b2Vec2 meanderDirection;
        b2Timer meanderTimer;

    public:
        Bunny();
        ~Bunny();

        static bool Initiate();
        static void Cleanup();

        bool Create(float x, float y, bool female, bool zombie, b2World* world);
        void Destroy(b2World* world);

        void Process();
        void Render(float scaleFactor, sf::RenderWindow* window);
        void RenderVision(float scaleFactor, sf::RenderWindow* window);

        void Pause();
        void Resume();

        void Impulse(float x, float y);
        void BecomeZombie();
        bool IsFemale();
        bool IsZombie();
        bool SeesBunny();
        bool SeesZombie();
        void BecomeAware(Bunny* otherBunny);
        void BecomeUnaware(Bunny* otherBunny);
        void BeginContact(Bunny* otherBunny);
        void EndContact(Bunny* otherBunny);
        b2Vec2 GetLocation();
        b2Vec2 GetVelocity();
        void Meander();
};

#endif
