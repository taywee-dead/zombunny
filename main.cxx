/*
 *  Lead Coder:     Taylor C. Richberger
 */

#include <cstdlib>
#include <ctime>
#include "core.hxx"

int main()
{
    srand(time(NULL));
    Core game("Zombunny (Built on " + std::string(__DATE__) + ")");
    return (game.Run());
}
