/*
 *  Lead Coder:     Taylor C. Richberger
 */

#ifndef BUNNY_CXX
#define BUNNY_CXX

#include <iostream>

#include "bunny.hxx"

Bunny::Bunny()
{
}

Bunny::~Bunny()
{
}

bool Bunny::Initiate()
{
    return (true);
}

void Bunny::Cleanup()
{
}

bool Bunny::Create(float x, float y, bool female, bool zombie, b2World* world)
{
    seesZombie = false;
    seesBunny = false;

    visibleBunnies = new std::vector<Bunny*>();
    contactBunnies = new std::vector<Bunny*>();

    this->female = female;
    this->zombie = zombie;
    do
    {
        meanderDirection.x = (rand() % 51) - 25;
        meanderDirection.y = (rand() % 51) - 25;
    } while ((meanderDirection.x == 0) && (meanderDirection.y == 0));
    b2BodyDef* bodyDef = new b2BodyDef();
    bodyDef->position.Set(x, y);
    b2CircleShape* bunnyShape = new b2CircleShape();
    bunnyShape->m_radius = 1.0f;

    b2CircleShape* visionShape = new b2CircleShape();
    visionShape->m_radius = 10.0f;

    b2FixtureDef* bunnyFixtureDef = new b2FixtureDef();
    bunnyFixtureDef->shape = bunnyShape;

    bunnyFixtureDef->density = 1.0f;
    bunnyFixtureDef->friction = 0.0f;
    bunnyFixtureDef->restitution = 0.025f;

    b2FixtureDef* visionFixtureDef = new b2FixtureDef();
    visionFixtureDef->shape = visionShape;
    visionFixtureDef->isSensor = true;

    visionFixtureDef->density = 0.0f;
    visionFixtureDef->friction = 0.0f;
    visionFixtureDef->restitution = 0.0f;

    bodyDef->type = b2_dynamicBody;
    bodyDef->linearDamping = 1.0f;
    body = world->CreateBody(bodyDef);
    body->SetFixedRotation(true);
    body->SetUserData((void*)this);
    bunnyFixture = body->CreateFixture(bunnyFixtureDef);
    visionFixture = body->CreateFixture(visionFixtureDef);
    bunnyFixture->SetUserData((void*)1);
    visionFixture->SetUserData((void*)2);

    renderBunny = sf::Shape::Circle(0.0f, 0.0f, 1.0f, sf::Color::White, 2.0f, sf::Color::White);
    renderBunny.EnableOutline(false);

    renderVision = sf::Shape::Circle(0.0f, 0.0, 10.0f, sf::Color::Green, 2.0f, sf::Color::Green);
    renderVision.EnableOutline(true);
    renderVision.EnableFill(false);
    if (!female)
    {
        renderBunny.SetColor(sf::Color::Cyan);
    }
    if (zombie)
    {
        renderBunny.SetColor(sf::Color::Green);
        if (female)
        {
            renderBunny.SetColor(sf::Color(255, 0, 255, 255));
        }
    }

    delete bodyDef;
    delete bunnyFixtureDef;
    delete visionFixtureDef;
    delete bunnyShape;
    delete visionShape;
    return (true);
}

void Bunny::Destroy(b2World* world)
{
    world->DestroyBody(body);
}

void Bunny::Process()
{
    b2Vec2 force(0.0f, 0.0f);
    seesBunny = false;
    seesZombie = false;

    if (zombie)
    {
        // Analyze all bunnies seen
        for (unsigned short int i = 0; i < visibleBunnies->size(); i++)
        {
            // If current bunny is not a zombie
            if (!visibleBunnies->at(i)->IsZombie())
            {
                // Set value for first seen bunny
                if (!seesBunny)
                {
                    force = body->GetLocalPoint(visibleBunnies->at(i)->GetLocation());
                    seesBunny = true;
                }

                // Find closest bunny this zombie sees
                if (sqrt(pow(body->GetLocalPoint(visibleBunnies->at(i)->GetLocation()).x, 2.0f) + pow(body->GetLocalPoint(visibleBunnies->at(i)->GetLocation()).y, 2.0f)) < sqrt(pow(force.x, 2.0f) + pow(force.y, 2.0f)))
                {
                    force = body->GetLocalPoint(visibleBunnies->at(i)->GetLocation());
                }
            }
        }

        if (!seesBunny)
        {
            for (unsigned short int i = 0; i < visibleBunnies->size(); i++)
            {
                // Chases same average direction as all visible zombies who see bunnies
                if (visibleBunnies->at(i)->SeesBunny())
                {
                    force += visibleBunnies->at(i)->GetVelocity();
                }
                seesZombie = true;
            }
        }


        // Prevent any possible divide by zero errors
        if ((force.x != 0.0f) || (force.y != 0.0f))
        {
            // Chase closest bunny
            body->ApplyForceToCenter((8.0f / sqrt(pow(force.x, 2.0f) + pow(force.y, 2.0f))) * force);
        } else
        // Wander randomly if this zombie isn't moving
        {
            Meander();
            body->ApplyForceToCenter((4.0f / sqrt(pow(meanderDirection.x, 2.0f) + pow(meanderDirection.y, 2.0f))) * meanderDirection);
        }
    } else
    {
        // Analyze all bunnies seen
        for (unsigned short int i = 0; i < visibleBunnies->size(); i++)
        {
            // If current bunny is a zombie
            if (visibleBunnies->at(i)->IsZombie())
            {
                // Find center of masses of visible zombies
                force += body->GetLocalPoint(visibleBunnies->at(i)->GetLocation());
                seesZombie = true;
            }
        }

        if (!seesZombie)
        {
            for (unsigned short int i = 0; i < visibleBunnies->size(); i++)
            {
                // Find center of masses of bunnies who see zombies
                if (visibleBunnies->at(i)->SeesZombie())
                {
                    force += body->GetLocalPoint(visibleBunnies->at(i)->GetLocation());
                    // seesBunny only refers to bunnies who see zombies at this point
                    seesBunny = true;
                }
            }
        }

        // If this bunny doesn't see a zombie or any bunnies who see zombies
        if (!seesZombie && !seesBunny)
        {
            for (unsigned short int i = 0; i < visibleBunnies->size(); i++)
            {
                // If current bunny is not the same gender as this bunny
                if (female != visibleBunnies->at(i)->IsFemale())
                {
                    // Set value for first seen bunny
                    if (!seesBunny)
                    {
                        force = -body->GetLocalPoint(visibleBunnies->at(i)->GetLocation());
                        seesBunny = true;
                    }

                    // Find closest opposite gender bunny this bunny sees
                    if (sqrt(pow(body->GetLocalPoint(visibleBunnies->at(i)->GetLocation()).x, 2.0f) + pow(body->GetLocalPoint(visibleBunnies->at(i)->GetLocation()).y, 2.0f)) < sqrt(pow(force.x, 2.0f) + pow(force.y, 2.0f)))
                    {
                        force = -body->GetLocalPoint(visibleBunnies->at(i)->GetLocation());
                    }
                }
            }
        }

        // If force is not zero
        if ((force.x != 0.0f) || (force.y != 0.0f))
        {
            // Run directly away from center of mass of zombies, defined by force
            // Will also run from bunnies who can see zombies, if no zombies ar visible
            body->ApplyForceToCenter(-(10.0f / sqrt(pow(force.x, 2.0f) + pow(force.y, 2.0f))) * force);
        }

    }
}

void Bunny::Render(float scaleFactor, sf::RenderWindow* window)
{
    renderBunny.SetScale(scaleFactor, scaleFactor);
    renderBunny.SetOutlineThickness(1.0f / scaleFactor);
    renderBunny.SetPosition(
        body->GetWorldPoint(((b2PolygonShape*)bunnyFixture->GetShape())->m_centroid).x * scaleFactor,
        body->GetWorldPoint(((b2PolygonShape*)bunnyFixture->GetShape())->m_centroid).y * scaleFactor);
    renderBunny.SetRotation(body->GetAngle() * 180.0f / 3.1415926535);

    window->Draw(renderBunny);
}

void Bunny::RenderVision(float scaleFactor, sf::RenderWindow* window)
{
    renderVision.SetScale(scaleFactor, scaleFactor);
    renderVision.SetOutlineThickness(1.0f / scaleFactor);
    // Gets the shape of the fixture, casts it to b2PolygonShape*, and recieves the
    //+m_centroid member, then translates it to the world's point, and scales it
    renderVision.SetPosition(
        body->GetWorldPoint(((b2PolygonShape*)visionFixture->GetShape())->m_centroid).x * scaleFactor,
        body->GetWorldPoint(((b2PolygonShape*)visionFixture->GetShape())->m_centroid).y * scaleFactor);
    renderVision.SetRotation(body->GetAngle() * 180.0f / 3.1415926535);

    window->Draw(renderVision);
}

void Bunny::Pause()
{
}

void Bunny::Resume()
{

}

void Bunny::Impulse(float x, float y)
{
    body->SetLinearVelocity(b2Vec2(x, y) + body->GetLinearVelocity());
}

void Bunny::BecomeZombie()
{
    // Makes bunny zombie
    zombie = true;
    renderBunny.SetColor(sf::Color::Green);

    if (female)
    {
        renderBunny.SetColor(sf::Color(255, 0, 255, 255));
    }

    // Make all contacting non-zombie bunnies zombies
    for (unsigned short int i = 0; i < contactBunnies->size(); i++)
    {
        if (!contactBunnies->at(i)->IsZombie())
        {
            contactBunnies->at(i)->BecomeZombie();
        }
    }
}

bool Bunny::IsFemale()
{
    return (female);
}

bool Bunny::IsZombie()
{
    return (zombie);
}

bool Bunny::SeesBunny()
{
    return (seesBunny);
}

bool Bunny::SeesZombie()
{
    return (seesZombie);
}

void Bunny::BecomeAware(Bunny* otherBunny)
{
    visibleBunnies->push_back(otherBunny);
}

void Bunny::BecomeUnaware(Bunny* otherBunny)
{
    for (unsigned short int i = 0; i < visibleBunnies->size(); i++)
    {
        if (otherBunny == visibleBunnies->at(i))
        {
            visibleBunnies->erase(visibleBunnies->begin() + i);
            break;
        }
    }
}

void Bunny::BeginContact(Bunny* otherBunny)
{
    contactBunnies->push_back(otherBunny);
    if (zombie)
    {
        otherBunny->BecomeZombie();
    }
}

void Bunny::EndContact(Bunny* otherBunny)
{
    for (unsigned short int i = 0; i < contactBunnies->size(); i++)
    {
        if (otherBunny == contactBunnies->at(i))
        {
            contactBunnies->erase(contactBunnies->begin() + i);
            break;
        }
    }
}

b2Vec2 Bunny::GetLocation()
{
    return(body->GetWorldCenter());
}

b2Vec2 Bunny::GetVelocity()
{
    return(body->GetLinearVelocity());
}

void Bunny::Meander()
{
    // Only changes directions every 3 seconds
    if (meanderTimer.GetMilliseconds() > 3000)
    {
        meanderTimer.Reset();
        // Sets each of meanderDirection's components to a random number ranging from -25 to 25,
        //+does not let both be zero
        do
        {
            meanderDirection.x = (rand() % 51) - 25;
            meanderDirection.y = (rand() % 51) - 25;
        } while ((meanderDirection.x == 0) && (meanderDirection.y == 0));
    }
}

#endif
